#include <iostream>

using namespace std;

int  negativeevennumbers(int numbers[], int n);
bool primenum(int number);
int  sumAllPrimes(int numbers[], int n);
int  pozitivenumber(int numbers [], int n);

int main()
{
    const  int n = 8;
    int numbers[n] = { -1,3,-8,4,15,-2,-23,13 };
    cout << negativeevennumbers(numbers, n) << endl;
    cout << pozitivenumber(numbers, n) << endl;
    cout << sumAllPrimes(numbers,  n) << endl;

    
}
int  negativeevennumbers(int numbers[], int n)
{
    int sum = 0;
    for (int i = 0; i < n; i++)
    {
        if (numbers[i] < 0 && numbers[i] % 2 == 0)
        {
            sum += numbers[i];
        }
    }
    return sum;
}
bool primenum (int number )
{
        if (number  < 1) 
        {
            return false;
        }
        for (int j = 2; j <= sqrt(number); j++)
            if (number  % j == 0)
            {
                return true;
            }
        return true ;
}
int pozitivenumber(int numbers[], int n) 
{
    int sum = 0;
    for (int i = 2; i < n; i++) 
    {
        if (primenum(i) && numbers[i] > 0) {
            sum += numbers[i];
        }
    }
    return sum;
}
int sumAllPrimes(int numbers[], int n) 
{
    int sum = 0;
    for (int i = 0; i < n; i++) 
    {

        if (primenum(numbers[i])) {
            sum += numbers[i];
       }
    }

    return sum;
}
