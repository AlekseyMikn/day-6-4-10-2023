#include <iostream>

using namespace std;

enum class Operation : char { Add = '+', Subtract = '-', Multiply = '*',Devision = '/' };

bool tryCalculate(int number1, int number2, Operation operation , int & result);


int main()
{
	int num;
	cout << "Enter number1 " ;
	cin >> num;
	int num2;
	cout << "Enter number2 " ;
	cin >> num2;
	char operation;
	cout << "Enter operation ";
	cin >> operation;
	int result = 0;
	if (tryCalculate(num, num2,(Operation) operation, result ))
	{
		cout << num << operation << num2 << '=' << result << endl;
	}
	else 
	{
		cout << "Data not valid" << endl;
	}
}
bool  tryCalculate(int number1 ,int number2 ,Operation operation,int & result) 
{
	bool  okey = true;
	switch (operation)
	{
	case Operation::Add:
		result = number1 + number2;
		break;
	case Operation::Devision:
		if (number2 == 0) 
		{
			okey = false;
		}
		else 
		{
			result = number1 / number2; 
		}
		break;
	case Operation::Subtract :
		result = number1 - number2;
		break;
	case Operation::Multiply:
		result  = number1 * number2;
		break;
	default:
		okey = false;
		break;
	}
	
	return okey;
}
