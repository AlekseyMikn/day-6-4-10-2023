#include <cmath>
#include <iostream>
using namespace std;
int main()
{
    int n;
    cin >> n;
    int i = 1;
    double s = 0;
    while (i <= n)
    {
        s = s + 1. / pow((2 * i) + 1, 2);
        i++;
    }
    cout << s;
}